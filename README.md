# Det danske folketing #

Den samlede data om danske folketingsmedlemmer ligger spredt over en række separate websider. Formålet med denne datasamling er, at få sat system i data, primært med fokus på folketingsmedlemmernes arbejdsliv før medlemsskab, karakteren af disse job og hvilket uddannelsesniveau et medlem har.

### Hvad skal det bruges til? ###

Når data er struktureret, så kan forstandige mennesker lave statistik over det. Det er dataindsamlerens ønske, at andre blot benytter data efter forgodtbefindende, eneste krav er en kildehenvisning til data.

### Hvordan kommer jeg igang? ###

Denne samling kan du hente på flere måder, hvor den nemmeste nok er, at lave en klon af respositoriet. Hvis du ikke ved, hvordan dette gøres, så enten hæng på, til jeg får lavet en simpel vejledning, alternativt tag fat i en lidt IT-kyndig ven, som kan hjælpe.

Et alternativ er, blot at lave copy/paste af den enkelte fils indhold, min lange tekniske erfaring viser dog, at dette også kan være svært for en del, og jeg yder ingen for for support.

### Bidrag til forbedring ###

Finder du fejl i data eller scripts, så er du velkommen til at lave et pull request, hvor du redegør for fejlen, og hvorfor din rettelse fikser det. For nogle medlemmer er data nemlig uklart pga. sammenfald mellem studier, arbejde, kurser (som ikke tæller som studie) eller blot en ukendt årrække angivet som "arbejdede som så og så".

### Copyright 2020 ###

Benytter du data til andet end private formål, så skal du angive kilden, enten ved mit navn Jan Oksfeldt Jonasen eller med link til dette repository. 
Private formål er f.eks. Facebook opslag, referencer, spas eller blot til at kigge i.
Benyttes de til kommercielle formål f.eks. aviser, kommercielle blogs, promovering, så skal der kildeangives.